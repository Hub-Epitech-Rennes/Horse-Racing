#!/usr/bin/env bash

if [ "$#" -ne 1 ]
then
    echo "USAGE : $0 horseNumber"
    exit
fi
./mouli/pytest.py $1 > file
cat file | ./mouli/pytest2.py > example
cat file | ./rendu.py > rendu
diff example rendu
if [ "$?" -ne 0 ]
then
    echo "Fail"
else
    echo "Good"
fi
rm -f example rendu file
