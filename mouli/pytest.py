#!/usr/bin/env python3
import random
import sys


MAX_POWER = 10000001
random.seed()
def verif(list_):
    list_.sort()
    i = len(list_) - 1
    for j in range(i):
        if (list_[j] == list_[j + 1]):
            return False
    return True

if len(sys.argv) < 2:
    print("Missing horse number", file=sys.stderr)
    exit()
horse = int(sys.argv[1])
print(horse)
flag = False
while flag == False:
    list_ = random.sample(range(1, MAX_POWER), horse)
    flag = verif(list_)
j = len(list_)
random.shuffle(list_)
for i in range(j) :
    print(list_[i])
